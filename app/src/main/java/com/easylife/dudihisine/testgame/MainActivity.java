package com.easylife.dudihisine.testgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO:Results List
    }

    public void startGame(View view) {

        startActivity(new Intent(this, GameActivity.class));

    }

    public void showCharectors(View view) {

        startActivity(new Intent(this, CharActivity.class));
    }
}
