package com.easylife.dudihisine.testgame;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.thunderrise.animations.PulseAnimation;

import java.util.Timer;
import java.util.TimerTask;

import xyz.hanks.library.SmallBang;

public class GameActivity extends AppCompatActivity {

    private TextView scoreLabel, startLabel;
    private ImageView fish, trap, diamond;

    //TODO:Trail lines after the Character

    //Sound
    private SoundPlayer soundPlayer;

    //Counters
    private int scoreCounter = 0;
    private int tempCounterAnimation = 100;
    private int tempCounterAnimation2 = 500;

    //Size
    private int frameH;

    //Positions
    private int fishX, fishY,
            diamondX, diamondY,
            trapX, trapY;

    //Speed
    private int fishSpeed, diamondSpeed, trapSpeed;

    //Initialize Class
    private Handler handler = new Handler();
    private Timer timer = new Timer();

    //Status Check
    private boolean action_flag = false;
    private boolean start_flag = false;

    //Player
    private GamePlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        Constants.SCREEN_HEIGTH = dm.heightPixels;
        Constants.SCREEN_WIDTH = dm.widthPixels;

        SharedPreferences settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        player = new GamePlayer(settings.getInt("SAVED_CHAR", R.drawable.anim_lenny),Constants.SCREEN_HEIGTH,(ImageView) findViewById(R.id.player_icon));
        scoreLabel = (TextView) findViewById(R.id.score_label);
        startLabel = (TextView) findViewById(R.id.start_label);
        fish = (ImageView) findViewById(R.id.fish_icon);
        trap = (ImageView) findViewById(R.id.trap_icon);
        diamond = (ImageView) findViewById(R.id.diamond_icon);


        //fish
        AnimatorSet fishSpinSet = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.spin_animate);
        fishSpinSet.setTarget(fish);
        fishSpinSet.start();

        soundPlayer = new SoundPlayer(this);

        //Now

        fishSpeed = Math.round(Constants.SCREEN_WIDTH / 70F);
        diamondSpeed = Math.round(Constants.SCREEN_WIDTH / 46F);
        trapSpeed = Math.round(Constants.SCREEN_WIDTH / 45F);

        //Move to out of screen
        fish.setX(-200);
        fish.setY(-200);
        diamond.setX(-200);
        diamond.setY(-200);
        trap.setX(-200);
        trap.setY(-200);

    }

    public boolean onTouchEvent(MotionEvent me) {

        if (!start_flag) {
            start_flag = true;

            FrameLayout frame = (FrameLayout) findViewById(R.id.frame);
            frameH = frame.getHeight();

            player.setPlayer();

            startLabel.setVisibility(View.GONE);

            if (timer != null) {
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                changePos();
                            }
                        });
                    }
                }, 0, 20);
            }

        } else {
            if (me.getAction() == MotionEvent.ACTION_DOWN) {
                action_flag = true;
            } else if (me.getAction() == MotionEvent.ACTION_UP) {
                action_flag = false;
            }
        }

        return true;
    }

    public void changePos() {

        hitCheck();

        //fish
        fishX -= fishSpeed;

        if (fishX < -(Constants.SCREEN_WIDTH + fish.getWidth())) {
            fishX = Constants.SCREEN_WIDTH + 20;
            fishY = (int) Math.floor(Math.random() * (frameH - fish.getHeight()));
            fish.setY(fishY);
        }

        fish.setX(fishX);


        //Pink
        diamondX -= diamondSpeed;

        if (diamondX < -(Constants.SCREEN_WIDTH + diamond.getWidth())) {
            diamondX = Constants.SCREEN_WIDTH + 5000;
            diamondY = (int) Math.floor(Math.random() * (frameH - diamond.getHeight()));
            diamond.setY(diamondY);
        }

        diamond.setX(diamondX);


        //trap
        trapX -= trapSpeed;

        if (trapX <= -(Constants.SCREEN_WIDTH + trap.getWidth())) {
            trapX = Constants.SCREEN_WIDTH + 10;
            trapY = (int) Math.floor(Math.random() * (frameH - trap.getHeight()));
            trap.setY(trapY);
        }

        trap.setX(trapX);

        //Move box
        if (action_flag) {
            //Touching
            player.jump();
        } else {
            //Releasing
            player.release();
        }

        //Check box position.
        if (player.getPlayerY() < 0) player.setPlayerY(0);
        if (player.getPlayerY() > frameH + player.getPlayerSize()) {
            gameOver();
            soundPlayer.playOverSound();
            return;
        }
    }

    private void hitCheck() {
        // If the center of the ball is in the box, it counts as hit.

        //fish
        if (player.checkHit((fishX + fish.getWidth() / 2),(fishY + fish.getHeight() / 2))) {
            fishX = Constants.SCREEN_WIDTH + 20;
            fishY = (int) Math.floor(Math.random() * (frameH - fish.getHeight()));
            doAfterHit(fish,fishY,20);
        }

        //diamond
        if (player.checkHit((diamondX + diamond.getWidth() / 2),(diamondY + diamond.getHeight() / 2))) {
            diamondX = Constants.SCREEN_WIDTH + 5000;
            diamondY = (int) Math.floor(Math.random() * (frameH - diamond.getHeight()));
            doAfterHit(diamond,diamondY,100);
        }

        //Trap
        if (player.checkHit((trapX + trap.getWidth() / 2),(trapY + trap.getHeight() / 2))) {
            gameOver();
            soundPlayer.playOverSound();

            return;
        }

        if(scoreCounter >= tempCounterAnimation){
            AnimationPulse(scoreLabel);
            tempCounterAnimation += 100 ;
        }

        if(scoreCounter >= tempCounterAnimation2){
            AnimationBlow(scoreLabel);
            fishSpeed += 2;
            diamondSpeed += 2;
            trapSpeed += 2;
            tempCounterAnimation2 += 500;
        }

        scoreLabel.setText(scoreCounter + "");

    }

    public void AnimationPulse(View view){
         PulseAnimation.create().with(view)
                .setDuration(250)
                .setRepeatMode(PulseAnimation.REVERSE)
                .setRepeatCount(1)
                .start();
    }

    public void AnimationBlow(View view){
        SmallBang mSmallBang = SmallBang.attach2Window(this);
        mSmallBang.setColors(new int[]{Color.LTGRAY, Color.WHITE, Color.DKGRAY});
        mSmallBang.setDotNumber(30);
        mSmallBang.bang(view);

    }

    private void doAfterHit(ImageView imageView,int y,int addUp){
        imageView.setY(y);
        scoreCounter += addUp;
        soundPlayer.playHitSound();
        AnimationPulse(player.playerIV);
    }

    private void gameOver() {

        //Stop timer
        timer.cancel();
        timer = null;

        //Show result
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("SCORE", scoreCounter);
        startActivity(intent);
        finish();

    }
}
