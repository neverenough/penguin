package com.easylife.dudihisine.testgame;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dudihisine on 14/04/2017.
 */

public class FragmentBackground extends Fragment {

    //Clouds & sun
    private ImageView cloud1;
    private ImageView cloud2;
    private ImageView cloud3;
    private ImageView sun;

    private FrameLayout frame;

    private Timer timer = new Timer();
    private Handler handler = new Handler();

    private int cloud1X, cloud1Y,
            cloud2X, cloud2Y,
            cloud3X, cloud3Y,
            sunX;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment, container, false);

        sun = (ImageView) view.findViewById(R.id.sun);
        cloud1 = (ImageView) view.findViewById(R.id.cloud1);
        cloud2 = (ImageView) view.findViewById(R.id.cloud2);
        cloud3 = (ImageView) view.findViewById(R.id.cloud3);
        frame = (FrameLayout) view.findViewById(R.id.background_frame);

        cloud1X = frame.getWidth() + cloud1.getWidth();
        cloud2X = frame.getWidth() + cloud1.getWidth();
        cloud3X = frame.getWidth() + cloud1.getWidth();

        cloud1.setY(100);
        cloud2.setY(300);
        cloud3.setY(400);

        cloud1X = 500;
        cloud2X = 750;
        cloud3X = 350;

        play();

        return view;
    }

    private void play(){

        final int frameH = frame.getHeight();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        //sun
                        sunX -= 2;

                        if (sunX < -(sun.getWidth())) {
                            sunX = Constants.SCREEN_WIDTH + 5000;
                        }

                        sun.setX(sunX);

                        //clouds
                        cloud1X -= 3;

                        if (cloud1X < -(cloud1.getWidth())) {
                            cloud1X = Constants.SCREEN_WIDTH + 140;
                            cloud1Y = (int) Math.floor(Math.random() * (frameH + cloud1.getHeight()));
                            cloud1.setY(cloud1Y);
                        }

                        cloud1.setX(cloud1X);

                        cloud2X -= 3.5;

                        if (cloud2X < -(cloud2.getWidth())) {
                            cloud2X = Constants.SCREEN_WIDTH + 200;
                            cloud2Y = (int) Math.floor(Math.random() * (frameH + cloud2.getHeight()));
                            cloud2.setY(cloud2Y);
                        }
                        cloud2.setX(cloud2X);

                        cloud3X -= 3.2;

                        if (cloud3X < -(cloud3.getWidth())) {
                            cloud3X = Constants.SCREEN_WIDTH + 110;
                            cloud3Y = (int) Math.floor(Math.random() * (frameH + cloud3.getHeight()));
                            cloud3.setY(cloud3Y);
                        }
                        cloud3.setX(cloud3X);
                    }
                });
            }
        }, 0, 20);
    }
}
