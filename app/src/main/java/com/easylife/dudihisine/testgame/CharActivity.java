package com.easylife.dudihisine.testgame;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class CharActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_char);

        findViewById(R.id.select_lenny).setOnClickListener(this);
        findViewById(R.id.select_bob).setOnClickListener(this);
        findViewById(R.id.select_brian).setOnClickListener(this);
        findViewById(R.id.select_jack).setOnClickListener(this);

        ImageView lenny = (ImageView) findViewById(R.id.lenny);
        ImageView bob = (ImageView) findViewById(R.id.bob);
        ImageView brian = (ImageView) findViewById(R.id.brian);
        ImageView jack = (ImageView) findViewById(R.id.jack);

        AnimationDrawable animlenny, animBob,animBrian,animJack;

        lenny.setBackgroundResource(R.drawable.anim_lenny);
        animlenny = (AnimationDrawable) lenny.getBackground();
        animlenny.start();

        bob.setBackgroundResource(R.drawable.anim_bob);
        animBob = (AnimationDrawable) bob.getBackground();
        animBob.start();

        brian.setBackgroundResource(R.drawable.anim_brian);
        animBrian = (AnimationDrawable) brian.getBackground();
        animBrian.start();

        jack.setBackgroundResource(R.drawable.anim_jack);
        animJack = (AnimationDrawable) jack.getBackground();
        animJack.start();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.select_lenny:
                changeChar(R.drawable.anim_lenny);
                Toast.makeText(this, "you pick lenny" , Toast.LENGTH_SHORT).show();
                break;
            case R.id.select_bob:
                changeChar(R.drawable.anim_bob);
                Toast.makeText(this, "you pick bob" , Toast.LENGTH_SHORT).show();
                break;
            case R.id.select_brian:
                changeChar(R.drawable.anim_brian);
                Toast.makeText(this, "you pick brian" , Toast.LENGTH_SHORT).show();
                break;
            case R.id.select_jack:
                changeChar(R.drawable.anim_jack);
                Toast.makeText(this, "you pick jack" , Toast.LENGTH_SHORT).show();
                break;

        }

        finish();

    }

    public void changeChar(int charector){

        //Save
        SharedPreferences settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("SAVED_CHAR", charector);
        editor.commit();
    }
}
