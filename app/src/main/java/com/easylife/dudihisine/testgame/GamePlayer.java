package com.easylife.dudihisine.testgame;

import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by dudihisine on 13/04/2017.
 */

public class GamePlayer {

    public ImageView playerIV;
    private AnimationDrawable animPenguin;
    private int playerX;
    private int playerY;
    private int playerSize;
    private int playerSpeed;


    public GamePlayer(int chosenPlayer, int screenH, ImageView imageView) {

        playerIV = imageView;
        this.playerX = 0;
        this.playerY = 500;

        playerIV.setX(playerX);
        playerIV.setY(playerY);

        this.playerSpeed = Math.round(screenH / 50F);
        playerIV.setBackgroundResource(chosenPlayer);
        animPenguin = (AnimationDrawable) playerIV.getBackground();

    }

    public void jump() {
        playerY -= playerSpeed;
        animPenguin.start();
        playerIV.setY(playerY);
    }

    public void release() {
        playerY += playerSpeed;
        animPenguin.stop();
        playerIV.setY(playerY);
    }

    public boolean checkHit(int x, int y) {
        if (0 <= x && x <= playerSize && playerY <= y && y <= (playerY + playerSize)) {
            return true;
        } else {
            return false;
        }
    }

    public int getPlayerY() {
        return playerY;
    }

    public void setPlayerY(int playerY) {
        this.playerY = playerY;
    }

    public int getPlayerSize() {
        return playerSize;
    }

    public void setPlayer() {
        this.playerSize = playerIV.getWidth();
        playerY = (int) playerIV.getY();
        Log.d("+++++++ ", playerSize + "");
    }
}
