package com.easylife.dudihisine.testgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private TextView scoreLabel, highScoreLabel;

    //TODO:Add Position in the Results list
    //TODO:Background

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        scoreLabel = (TextView) findViewById(R.id.game_over_score);
        highScoreLabel = (TextView) findViewById(R.id.game_over_high_score);

        int score = getIntent().getIntExtra("SCORE", 0);
        scoreLabel.setText(score + "");

        SharedPreferences settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        int highScore = settings.getInt("HIGH_SCORE", 0);

        if(score > highScore){
            highScoreLabel.setText("High Score : " + score);

            //Save
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE", score);
            editor.commit();

        }else{
            highScoreLabel.setText("High Score : " + highScore);

        }


    }

    public void tryAgain(View view){
        startActivity(new Intent(this,GameActivity.class));
        finish();
    }

    public void returnToMenu(View view) {
        finish();
    }
}
